import { Component, OnInit, EventEmitter } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";

import { Car } from "../../../../models/car";
import { CarService } from "../../services/car.service";
import { finalize } from "rxjs/operators";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {
  submitted = false;
  carTypes = ["Minivan", "Sedan", "Bus"];
  private _carForm: FormGroup;
  private _car: Car;
  private _cars: Car[];

  constructor(private carService: CarService) {}

  ngOnInit() {
    this._carForm = new FormGroup({
      id: new FormControl(""),
      vehicleId: new FormControl("", [
        Validators.required,
        Validators.maxLength(17),
        Validators.minLength(17)
      ]),
      name: new FormControl("", [
        Validators.required,
        Validators.maxLength(20),
        Validators.minLength(2)
      ]),
      model: new FormControl("", [
        Validators.required,
        Validators.maxLength(20),
        Validators.minLength(2)
      ]),
      year: new FormControl("", [
        Validators.required,
        Validators.min(1900),
        Validators.max(2019),
        Validators.maxLength(4)
      ]),
      carType: new FormControl(""),
      isNew: new FormControl("")
    });
    this.getCars();
  }

  /**
   * Method for get selected item from drop down
   * @param type
   */
  changeCarType(type: string) {
    const subStr = type.replace(/^.{3}/, "");
    this._carForm.get("carType").setValue(subStr, {
      onlySelf: true
    });
  }

  /**
   * Get data from Car service
   */
  getCars() {
    this.carService
      .getCars()
      .pipe(finalize(() => {}))
      .subscribe(cars => {
        this._cars = cars;
      });
  }

  /**
   * Add new car to car dashboard
   */
  addNewCar() {
    this._car = {
      vehicleId: undefined,
      name: undefined,
      model: undefined,
      year: undefined,
      isNew: true,
      carType: undefined
    };
    this._carForm.get("vehicleId").setValue(this._car.vehicleId);
    this._carForm.get("name").setValue(this._car.name);
    this._carForm.get("model").setValue(this._car.model);
    this._carForm.get("year").setValue(this._car.year);
    this._carForm.get("carType").setValue(this._car.carType);
    this._carForm.get("isNew").setValue(this._car.isNew);
  }

  /**
   * Get selected car item from select event on child component
   * @param data
   */
  onSelect(data: Car) {
    this._car = data;
    this._carForm.get("id").setValue(data.id);
    this._carForm.get("vehicleId").setValue(data.vehicleId);
    this._carForm.get("name").setValue(data.name);
    this._carForm.get("model").setValue(data.model);
    this._carForm.get("year").setValue(this._car.year);
    this._carForm.get("carType").setValue(this._car.carType);
    this._carForm.get("isNew").setValue(this._car.isNew);
  }

  /**
   * Method for hide change part of view by Close button click
   */
  hideItemCarForm() {
    this._car = undefined;
  }

  /**
   * Summit data from form
   */
  onSubmit() {
    const carData = this._carForm.value;
    this.submitted = true;
    if (this._carForm.invalid) {
      return;
    }
    if (carData.isNew) {
      this._cars.push(this._carForm.value);
    }
    if (!carData.isNew) {
      const carIndex = this._cars.findIndex(car => car.id === carData.id);
      this._cars.splice(carIndex, 1, carData);
    }
    this._carForm.reset();
    this._car = undefined;
  }

  /**
   * Method for showing error massage user if input data шы цкщтп
   * @param controlName
   */
  getErrorMessage(controlName: string): string {
    const fieldName = this._carForm.controls[controlName];
    switch (controlName) {
      case "vehicleId":
        if (fieldName.hasError("required")) {
          return "Name is required";
        }
        if (fieldName.hasError("minlength")) {
          return "Minimum length for a Vehicle Id is 17 characters";
        }
        if (fieldName.hasError("maxlength")) {
          return "Maximum length for Vehicle Id is 17 characters";
        }
        break;
      case "name":
        if (fieldName.hasError("required")) {
          return "Name is required";
        }
        if (fieldName.hasError("minlength")) {
          return "Minimum length for a name is 2 characters";
        }
        if (fieldName.hasError("maxlength")) {
          return "Maximum length for name is 16 characters";
        }
        break;
      case "model":
        if (fieldName.hasError("required")) {
          return "Name is required";
        }
        if (fieldName.hasError("minlength")) {
          return "Minimum length for a model is 2 characters";
        }
        if (fieldName.hasError("maxlength")) {
          return "Maximum length for model is 20 characters";
        }
        break;
      case "year":
        if (fieldName.hasError("required")) {
          return "Year is required";
        }
        if (fieldName.hasError("min")) {
          return "Minimum value for year is 1900";
        }
        if (fieldName.hasError("max")) {
          return "Maximum value for year is 2019";
        }
        if (fieldName.hasError("maxlength")) {
          return "Maximum length for year is 4 number";
        }
        break;
      default:
        return "Unknown value";
    }
  }
}
