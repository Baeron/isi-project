import { NgModule } from "@angular/core";
import { CoreRoutingModule } from "./core-routing.module";

import { CarService } from "./services/car.service";

@NgModule({
  declarations: [],
  imports: [CoreRoutingModule],
  providers: [CarService]
})
export class CoreModule {}
