import { Injectable } from "@angular/core";

import { Car } from "../../../models/car";
import { CARS } from "../../../mock/dummy-cars";
import { Observable, of } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class CarService {
  constructor() {}
  getCars(): Observable<Car[]> {
    return of(CARS);
  }
}
