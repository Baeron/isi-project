import { Component, EventEmitter, Output, Input } from "@angular/core";
import { Car } from "../../../../models/car";

@Component({
  selector: "app-table-header",
  templateUrl: "./table-header.component.html",
  styleUrls: ["./table-header.component.css"]
})
export class TableHeaderComponent {
  columns = ["vehicleId", "name", "model", "year", "carType"];
  tHeader = ["Vehicle Id", "Name", "Model", "Year", "Type"];
  @Input() _cars: Car[];
  @Output() onSelect = new EventEmitter<Car>();

  /**
   * Method to return selected car element to parent component
   * @param itemCar
   */
  onSelected(itemCar: Car) {
    this.onSelect.emit(itemCar);
  }
}
