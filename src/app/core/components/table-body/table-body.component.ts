import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Car } from "../../../../models/car";

@Component({
  selector: "[app-table-body]",
  templateUrl: "./table-body.component.html",
  styleUrls: ["./table-body.component.css"]
})
export class TableBodyComponent {
  @Input() car: Car;
  @Input() columns: string[];
  @Output() onSelect = new EventEmitter<any>();

  selectRow(row) {
    this.onSelect.emit(row);
  }
}
