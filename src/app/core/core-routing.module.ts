import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { DashboardComponent } from "./containers";

import {
  TableBodyComponent,
  TableHeaderComponent,
  TabComponent,
  TabsComponent
} from "./components";

const coreRoutes: Routes = [
  {
    path: "",
    component: DashboardComponent
  }
];

@NgModule({
  declarations: [
    DashboardComponent,
    TableBodyComponent,
    TableHeaderComponent,
    TabComponent,
    TabsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forRoot(coreRoutes),
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [RouterModule]
})
export class CoreRoutingModule {}
