import { Car } from "../models/car";

export const CARS: Car[] = [
  {
    id: 1,
    vehicleId: "4USBT53544LT26841",
    name: "BMW",
    model: "Z4 3.0i",
    year: "2004",
    isNew: false,
    carType: "Sedan"
  },
  {
    id: 2,
    vehicleId: "KL1UF756E6B195928",
    name: "CHEVROLET",
    model: "Rezzo 1.6",
    year: "2006",
    isNew: false,
    carType: "Minivan"
  },
  {
    id: 3,
    vehicleId: "ZFA18800004473122",
    name: "First",
    model: "First",
    year: "NOW",
    isNew: false,
    carType: "Sedan"
  },
  {
    id: 4,
    vehicleId: "JHLRE48577C415490",
    name: "FIAT",
    model: "Punto 80 ELX",
    year: "2002",
    isNew: false,
    carType: "Sedan"
  },
  {
    id: 5,
    vehicleId: "KNDJC733545301768",
    name: "HONDA",
    model: "CR-V 2.4 RVSi",
    year: "2007",
    isNew: false,
    carType: "Bus"
  }
];
