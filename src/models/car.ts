export interface Car {
  id?: number;
  vehicleId: string;
  name: string;
  model: string;
  year: string;
  isNew: boolean;
  carType: string;
}
